﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain
{
    public class PartnerExtension : IPartnerExtension
    {
        public async Task<int> ResetNumberIssuedPromoCodes() => 0;

    }
}
