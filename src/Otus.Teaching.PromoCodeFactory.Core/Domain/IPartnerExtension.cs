﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain
{
    public interface IPartnerExtension
    {
        Task<int> ResetNumberIssuedPromoCodes();
    }
}
