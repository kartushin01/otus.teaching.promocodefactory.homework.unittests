﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain
{
    public class CurrentDateTimeProvider : ICurrentDateTimeProvider
    {
        public DateTime CurrentDateTime => DateTime.Now;
    }
}
