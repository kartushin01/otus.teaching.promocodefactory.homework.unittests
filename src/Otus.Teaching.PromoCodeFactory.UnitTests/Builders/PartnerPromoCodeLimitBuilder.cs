﻿using System;
using System.Collections.Generic;
using System.Text;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.Builders
{
    public static class PartnerPromoCodeLimitBuilder
    {
        public static SetPartnerPromoCodeLimitRequest CreateBaseRequest()
        {

            SetPartnerPromoCodeLimitRequest request =
                new SetPartnerPromoCodeLimitRequest()
                {
                    EndDate = new DateTime(2020, 9, 1), 
                    Limit = 1000
                };

            return request;

        }

        public static SetPartnerPromoCodeLimitRequest  SetWhithOutLimit(this SetPartnerPromoCodeLimitRequest request)
        {
            request.Limit = 0;

            return request;
        }

    }
}
