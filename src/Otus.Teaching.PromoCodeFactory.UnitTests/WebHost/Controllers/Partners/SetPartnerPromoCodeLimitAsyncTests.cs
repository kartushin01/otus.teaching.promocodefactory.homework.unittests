﻿using System;
using System.Collections.Generic;
using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.UnitTests.Builders;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests
    {
        private readonly Mock<IRepository<Partner>> _partnersRepositoryMock;
        private readonly PartnersController _partnersController;
        private Mock<IPartnerExtension> _partnerExtensionMock;
        private readonly Mock<ICurrentDateTimeProvider> _currentDateTimeProvider;

        public SetPartnerPromoCodeLimitAsyncTests()
        {
            
            var fixture = new Fixture().Customize(new AutoMoqCustomization());

            _partnersRepositoryMock = fixture.Freeze<Mock<IRepository<Partner>>>();
            _partnerExtensionMock = fixture.Freeze<Mock<IPartnerExtension>>();
            _currentDateTimeProvider = fixture.Freeze<Mock<ICurrentDateTimeProvider>>();
            _partnersController = fixture.Build<PartnersController>().OmitAutoProperties().Create();
        }

        public SetPartnerPromoCodeLimitRequest CreateBasePartnerRequest() => PartnerPromoCodeLimitBuilder.CreateBaseRequest();

        public Partner CreateBasePartner() => PartnerBuilder.CreateBasePartner();

      
        [Theory]
        [InlineData("def47943-7aaf-44a1-ae21-05aa4948b165")]
        public async void SetPartnerPromoCodeLimitAsync_PartnerIsNull_ReturnsNotFound(string id)
        {
            // Arrange
            var partnerId = Guid.Parse(id);

            var request = CreateBasePartnerRequest();

            Partner partner = null;

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync((Partner) null);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            // Assert
            result.Should().BeAssignableTo<NotFoundResult>();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_ParnterIsNotActive_ShouldReturnBadRequest()
        {
            // Arrange
            Partner partner = CreateBasePartner().IsNotActive();

            var request = CreateBasePartnerRequest();

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, request);

            //Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();

        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_ResetCountsPromocodes_SholCallResetNumberIssuedPromoCodesMethod()
        {

            //Arrange
            Partner partner = CreateBasePartner().WhithIssuedPromoCodes(100);
            
            var request = CreateBasePartnerRequest();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);

            _partnerExtensionMock.Setup(ex =>
                ex.ResetNumberIssuedPromoCodes());

          
            _currentDateTimeProvider.Setup(x => x.CurrentDateTime).Returns(new DateTime(2020,08,20));

            // Act
            var result = _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, request);

            //Assert
            _partnerExtensionMock.Verify(x=>x.ResetNumberIssuedPromoCodes(), Times.Once);
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_CancelPreviousLimit_SholdCallCurrentDateTime()
        {

            //Arrange
            Partner partner = CreateBasePartner().WhithIssuedPromoCodes(100);

            var request = CreateBasePartnerRequest();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);

            _partnerExtensionMock.Setup(ex =>
                ex.ResetNumberIssuedPromoCodes());

            _currentDateTimeProvider.Setup(x => x.CurrentDateTime).Returns(new DateTime(2020, 08, 20));

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, request);

            //Assert
            _currentDateTimeProvider.Verify(x => x.CurrentDateTime, Times.Once);
        }


        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_LimitNoMoreThanZero_SholdReturnBadRequestObjectResult()
        {

            //Arrange

            Partner partner = CreateBasePartner().WhithIssuedPromoCodes(100);

            var request = CreateBasePartnerRequest().SetWhithOutLimit();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);
                

            _partnerExtensionMock.Setup(ex =>
                ex.ResetNumberIssuedPromoCodes());

            _currentDateTimeProvider.Setup(x => x.CurrentDateTime).Returns(new DateTime(2020, 08, 20));

            // Act

            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, request);

            //Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_SameLimitInDb_SholdCallUpdateAsync()
        {

            //Arrange
            Partner partner = CreateBasePartner().WhithIssuedPromoCodes(100);

            var request = CreateBasePartnerRequest();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);


            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, request);

            //Assert
           _partnersRepositoryMock.Verify(x=> x.UpdateAsync(partner));
        }



    }
}